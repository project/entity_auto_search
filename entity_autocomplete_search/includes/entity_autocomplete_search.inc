<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * @return type
 * 
 */
function manage_entity_auto_search() {
  return drupal_get_form('entity_autocomplete_search');
}

/**
 * @file 
 * @param type $form
 * @param type $form_state
 * @return type
 */
function entity_autocomplete_search($form, $form_state) {
  $placeholder = variable_get('eas_single_placeholder');
  $submit = variable_get('eas_single_submit');
  $form['single_autocomplete'] = array(
    '#type' => 'textfield',
    '#title' => '',
    '#attributes' => array('placeholder' => $placeholder),
    '#autocomplete_path' => 'entity/autocomplete/search',
  );
  if ($submit[1] != 0) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Search'),
    );
  }
  return $form;
}

/**
 * 
 * @param type $string
 */
function select_entity_type_node($string) {
  $matches = array();
  $bundle = array_filter(variable_get('eas_single_bundle'));
  $submit = variable_get('eas_single_submit');
  $details = variable_get('eas_single_details');
  $result = db_select('node', 'n');
  $result->fields('n', array('nid', 'title'));
  if (!empty($details['body'])) {
    $result->fields('fdb', array('body_value'));
    $result->leftJoin('field_data_body', 'fdb', 'n.nid = fdb.entity_id');
  }
  $result->condition('n.title', '%' . db_like($string) . '%', 'LIKE');
  $result->condition('n.type', $bundle, 'IN');
  $result->range(0, 10);
  $results = $result->execute();
  if (!empty($results)) {
    foreach ($results as $row) {
      if ($submit[1] != 0) {
        if (!empty($details['body'])) {
          $matches[check_plain($row->title)] = "<div><h3>" . check_plain($row->title) . "</h3><span>" . substr($row->body_value, 0, 300) . "</span></div>";
        }
        else {
          $matches[check_plain($row->title)] = "<h3>" . check_plain($row->title) . "</h3>";
        }
      }
      else {
        if (!empty($details['body'])) {
          $content .= "<div><a href='" . url('node/' . $row->nid) . "'><h3>" . check_plain($row->title) . "</h3></a><span>" . substr($row->body_value, 0, 300) . "</span></div>";
        }
        else {
          $content .= "<div><a href='" . url('node/' . $row->nid) . "'><h3>" . check_plain($row->title) . "</h3></a></div>";
        }
      }
      $matches['result'] = $content;
    }
  }
  drupal_json_output($matches);
}

/**
 * 
 * @param type $form
 * @param type $form_state
 */
function entity_autocomplete_search_submit($form, $form_state) {
  $submit = variable_get('eas_single_submit');
  if ($submit[1] != 0) {
    $title = $form_state['values']['single_autocomplete'];
    $bundle = array_filter(variable_get('eas_single_bundle'));
    $result = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->condition('n.title', db_like($title), 'LIKE')
        ->condition('n.type', $bundle, 'IN')
        ->execute()->fetchField();
    drupal_goto('node/' . $result);
  }
}
