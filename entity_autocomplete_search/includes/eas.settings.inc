<?php

/**
 * @file 
 * @param type $form
 * @param type $form_state
 * @return type
 */
function entity_autocomplete_search_single_block($form, &$form_state) {
    $type = content_type_option();
    $form['Multiple_content_Type_autocomplete'] = array(
        '#type' => 'fieldset',
        '#title' => t('Select Multiple content Types for Single Autocomplete Search Block'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );
    $form['Multiple_content_Type_autocomplete']['eas_single_bundle'] = array(
        '#type' => 'checkboxes',
        '#options' => $type,
        '#title' => t('List of content Types'),
        '#default_value' => variable_get('eas_single_bundle', array()),
    );
    $form['Multiple_content_Type_autocomplete']['eas_single_details'] = array(
        '#type' => 'checkboxes',
        '#options' => array("nid" => "Title", "body" => "description"),
        '#title' => t('Items display on the autocomplete search list'),
        '#default_value' => variable_get('eas_single_details', array("nid" => "Title")),
    );
    $form['Multiple_content_Type_autocomplete']['eas_single_placeholder'] = array(
        '#type' => 'textfield',
        '#title' => t('Placeholder of autocomplete search'),
        '#default_value' => variable_get('eas_single_placeholder', array()),
    );
    $form['Multiple_content_Type_autocomplete']['eas_single_submit'] = array(
        '#type' => 'checkboxes',
        '#options' => array('1' => "Yes"),
        '#title' => t('Select submit button after autocomplete search result'),
        '#default_value' => variable_get('eas_single_submit', array()),
    );
	      /**************solr********************************************/
    
    $form['config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Solr Configuration'),
    '#collapsible' => TRUE,
  );

  $form['config']['bundles'] = array(
    '#type' => 'markup',
    '#markup' => t('Select the entity types and bundles that should be indexed.'),
  );
  $form['config']['env_id'] = array(
    '#type' => 'value',
    '#value' => 'solr',
  );
  foreach (entity_get_info() as $entity_type => $entity_info) {
        if (!empty($entity_info['apachesolr']['indexable'])) {
            $options = array();
            foreach ($entity_info['bundles'] as $key => $info) {
                $options[$key] = $info['label'];
            }
            asort($options);
        }
    }
  $form['config']['eas_solar_submit'] = array(
        '#type' => 'checkboxes',
        '#title' => check_plain('Bundles on solr'),
        '#options' => $options,
        '#default_value' => variable_get('eas_solar_submit', array()),//apachesolr_get_index_bundles('solr', $entity_type),
      );
    return system_settings_form($form);
}

/**
 * 
 * @return type
 */
function content_type_option() {
    $query = db_select('node_type', 'nt')
                    ->fields('nt', array('type', 'name'))->execute()->fetchAll();
    $type = array();
    if (!empty($query)) {
        foreach ($query as $result) {
            $type[$result->type] = $result->name;
        }
    }
    return $type;
}
